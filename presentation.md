# Webpack

![what-is](what-is-webpack.png)

---

## First demo

`npm install -g webpack`

watchmode

---

## Second demo

`webpack.config.js`

`watch: true`

---

## webpack-dev-server

OOTB auto-refresh, diff-updates (no compiling everything every time).

Files saved in memory, nothing saved to disk (no bundle.js).

`npm install webpack-dev-server -g`

`webpack-dev-server`

http://localhost:8080/webpack-dev-server/

http://localhost:8080/ (no hot-loading)

(or: `webpack-dev-server --inline` for default hot loading, no status bar)

---

## Third demo

`require()`ing logger.js

---

## Fourth demo

A complete Angular 1 + webpack example project, with tests

---

## More webpack "what's possible"

https://github.com/webpack/webpack#a-small-example-of-whats-possible

---

## No Bower

 1. `npm install angular`
 2. `const angular = require('angular');` or `import angular from 'angular';`

---

## Assets

`import 'font-awesome/css/font-awesome.css';`

`import 'lato-webfont/fonts/Lato-Hairline.woff';`

`import '../styles/app.scss';`

---

## Caveats

---

### Dependencies need to [UMD'd](https://github.com/umdjs/umd) for easy use.

(Or else, you'll need to specify the entrypoint, webpack will be dumb, etc.)

### Require everything

app.controllers.js:

```
import './controllers/some1Ctrl';
import './controllers/some2Ctrl';
import './controllers/some3Ctrl';
import './controllers/some4Ctrl';
import './controllers/some5Ctrl';
import './controllers/some6Ctrl';
import './controllers/some7Ctrl';
import './controllers/some8Ctrl';
import './controllers/some9Ctrl';
```

(although you could use `require.context`)

---

### Testing

Slow, annoying with angular, annoying karma config setup

test.webpack.js in src:

```
// This file is an entry point for angular tests
// Avoids some weird issues when using webpack + angular.

import 'angular';
import './app';

import 'angular-mocks/angular-mocks';

const context = require.context('.', true, /.spec.js$/);

context.keys().forEach(context);
```

---

## Using SASS

app.js:

```
import './app.scss'
```

app.scss:

```
@import '~normalize.css/normalize';

@import 'core/button';
```

---

## Loaders

Know how to use different asset types (files, html, sass, etc) when being `require()`d.

order:

 1. preloaders specified in the configuration
 2. loaders specified in the configuration
 3. loaders specified in the request (e.g. `require('raw!./file.js')`)
 4. postLoaders specified in the configuration

---

## Plugins

Transform the whole bundle (not just individual assets), more powerful

webpack.config.js:

```
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var CopyWebpackPlugin = require('copy-webpack-plugin');
var OfflinePlugin = require('offline-plugin');
```

---

Using webpack like a pro:

```
[... tasks ...]
  "build": "rimraf dist && webpack --bail --progress --profile",
  "server": "webpack-dev-server --history-api-fallback --inline --progress",
  "test": "karma start",
  "test-watch": "karma start --auto-watch --no-single-run",
  "start": "npm run server",
[/... tasks ...]
```

---

## Webpack 2

 1. Native(!) ES6 Modules
 2. Native ES6(!) Code Splitting
 3. Dynamic expressions after built
 4. Better performance

"Imminent release" for last year...